<?php
add_action( 'admin_menu', 'nemcwpcore_add_admin_menu' );
add_action( 'admin_init', 'nemcwpcore_settings_init' );


function nemcwpcore_add_admin_menu(  ) {

	add_menu_page(
		'NEMC WP Core Support & Settings',
		'NEMC WP Core',
		'manage_options',
		'nemc_wp_core',
		'nemcwpcore_options_page',
		plugins_url( '../assets/menu_icon.png', __FILE__ )
	);

}


function nemcwpcore_settings_init(  ) {

	register_setting( 'nemc_settings_page', 'nemcwpcore_settings' );

	add_settings_section(
		'nemcwpcore_nemc_settings_page_section',
		__( 'Website Designed & Developed by Northeast Media Collection', 'nemcwpcore' ),
		'nemcwpcore_settings_section_callback',
		'nemc_settings_page'
	);

	add_settings_field(
		'nemcwpcore_checkbox_field_0',
		__( 'Disable Login Logo', 'nemcwpcore' ),
		'nemcwpcore_checkbox_field_0_render',
		'nemc_settings_page',
		'nemcwpcore_nemc_settings_page_section'
	);

	add_settings_field(
		'nemcwpcore_checkbox_field_1',
		__( 'Disable Clean Admin Bar', 'nemcwpcore' ),
		'nemcwpcore_checkbox_field_1_render',
		'nemc_settings_page',
		'nemcwpcore_nemc_settings_page_section'
	);

	add_settings_field(
		'nemcwpcore_checkbox_field_2',
		__( 'Disable Footer Text', 'nemcwpcore' ),
		'nemcwpcore_checkbox_field_2_render',
		'nemc_settings_page',
		'nemcwpcore_nemc_settings_page_section'
	);


}


function nemcwpcore_checkbox_field_0_render(  ) {
	$options = get_option( 'nemcwpcore_settings' );
	?>
	<input type='checkbox' name='nemcwpcore_settings[nemcwpcore_checkbox_field_0]' <?php checked( $options['nemcwpcore_checkbox_field_0'], 1 ); ?> value='1'>
	<?php
}
function nemcwpcore_checkbox_field_1_render(  ) {
	$options = get_option( 'nemcwpcore_settings' );
	?>
	<input type='checkbox' name='nemcwpcore_settings[nemcwpcore_checkbox_field_1]' <?php checked( $options['nemcwpcore_checkbox_field_1'], 1 ); ?> value='1'>
	<?php
}
function nemcwpcore_checkbox_field_2_render(  ) {
	$options = get_option( 'nemcwpcore_settings' );
	?>
	<input type='checkbox' name='nemcwpcore_settings[nemcwpcore_checkbox_field_2]' <?php checked( $options['nemcwpcore_checkbox_field_2'], 1 ); ?> value='1'>
	<?php
}


function nemcwpcore_settings_section_callback(  ) {
	echo __( '<p>For website support, please email <a href="mailto:neilparnold@gmail.com">Neil Arnold</a> or <a href="mailto:jspooner@maine.rr.com">Jason Spooner</a></p>', 'nemcwpcore' );
	echo __('<br /><hr />');
}

function nemcwpcore_options_page(  ) {

	?>
	<div class="wrap">
		<img src="<?php echo plugins_url( '../assets/nemc_logo.png', __FILE__ )?>" style="float:right;" />
		<h1>Northeast Media Collective</h1>

		<form action='options.php' method='post'>
			<?php
			settings_fields( 'nemc_settings_page' );
			do_settings_sections( 'nemc_settings_page' );
			submit_button();
			?>
		</form>
	</div>
	<?php

}

?>
