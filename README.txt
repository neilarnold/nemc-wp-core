=== Plugin Name ===
Contributors: neilarnold787287699
Donate link: www.neilarnold.com
Tags: nemc
Requires at least: 4.6
Tested up to: 4.6.1
Stable tag: 4.6.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Some standard functions in all projects.  

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.


== Installation ==

1. Upload `nemc-wp-cpre.php` and other files to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Changelog ==

= 1.0 =
* Initial Development


