<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @link       www.neilarnold.com
 * @since      1.0.0
 *
 * @package    Nemc_Wp_Core
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
