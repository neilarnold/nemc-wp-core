<?php

/**
 * Plugin Name:       NNEMC WP Core
 * Plugin URI:        www.northeastmediacollective.com
 * Description:       Standard functions for all Northeast Media Collective projects.
 * Version:           1.0.0
 * Author:            Neil Arnold
 * Author URI:        www.neilarnold.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nemc-wp-core
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) { die; }


function nemcwpcore_init() {
    include_once('includes/nemc-wp-core-settings.php');
}
add_action('init', 'nemcwpcore_init');


// Remove some of the crap from the admin bar
function nemcwpcore_remove_admin_bar_links() {
  if ( ! is_admin() ) {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('wpseo-menu');
    $wp_admin_bar->remove_menu('vc_inline-admin-bar-link');
    $wp_admin_bar->remove_menu('revslider');
  }
}
add_action( 'wp_before_admin_bar_render', 'nemcwpcore_remove_admin_bar_links' );


function nemcwpcore_login_logo() {
  $disable = get_option( 'nemcwpcore_settings' );
  if ($disable[nemcwpcore_checkbox_field_0] == '1')
    return;
  ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url( <?php echo plugins_url( 'assets/nemc_wp_logo.png', __FILE__ ) ?> );
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'nemcwpcore_login_logo' );


function nemc_admin_footer () {
  echo __('Website Designed & Developed by <a href="http://www.northeastmediacollective.com" target="_target">Northeast Media Collective</a>');
}
add_filter('admin_footer_text', 'nemc_admin_footer');
