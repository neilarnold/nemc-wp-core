## Author

Neil Arnold  
neilparnold@gmail.com  
www.neilarnold.com  

## Synopsis

Standard functions and procedures I like to include in all projects.  Config screen to turn them on and off.

## Motivation

I got tired of constantly remembering where my most recent version of this code was.  

## Installation

Justs install the plugin and activate.

## API Reference

None.

## License

GPLv2
